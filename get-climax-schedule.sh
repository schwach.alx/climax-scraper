#!/bin/bash
# This script attempts to parse the upcoming hockey schedule for the Colorado Climax team
TEAM_ID='790433'
SEASON_ID='20956'
#TODO: Will need to find someway of determining the current season_id automatically. Will be big pain in ass if it requires manual resolution and updating every season
PS_SCHEDULE_HOST='stats.pointstreak.com'
PS_SCHEDULE_ENDPOINT='players/players-team-schedule.html'
CLIMAX_SCHEDULE_URL="http://${PS_SCHEDULE_HOST}/${PS_SCHEDULE_ENDPOINT}?teamid=${TEAM_ID}&seasonid=${SEASON_ID}"
function echoerr { printf "%s\n" "$*" >&2 ; }

REQUIRED_UTILITIES=(curl pup jq)

# Check for required commands/utilities
for util in "${REQUIRED_UTILITIES[@]}"; do
    if [[ $(command -v "${util}" > /dev/null; echo $?) -ne 0 ]]; then
        echoerr "Missing required utility '${util}'! Exiting..."
        exit 1
    fi
done

### FUNCTIONS
# Returns 1 if the game row json given is a past game, 0 if game is in the future
function is-game-complete {
    # If .children[0].children[0].tag == "span", game is in the future
    if [[ "$(echo "${1}" | jq -r '.children[0].children[0].tag')" == "span" ]]; then
        echo 0
    else
        echo 1
    fi
}

# Extracts only the Home Team Name value from a given schedule row (must be converted to json with pup)
function extract-home-team-from-row-json {
    # If the game is already complete, Home team will be the 2nd child - 1st child if game is upcoming
    # We can check the state for this by looking at the tag of the second child, which will always have a 'tag' field
    # If the tag field is "a", this game is upcoming, if it is "b" it is complete
#    echoerr "DEBUG:home: ${1}"
    local tag_field=$(echo "${1}" | jq -r '.children[0].children[1].tag')

    if [[ "${tag_field}" == "a" ]]; then 
        # This game has not occurred yet - Home Team is in the 'text' field of the second element
        echo "${1}" | jq -r '.children[0].children[1].text'
    elif [[ "${tag_field}" == "b" ]]; then 
        # This game is over - Home Team is in the 'text' field of the first element
        echo "${1}" | jq -r '.children[0].children[0].text'
    else 
        echoerr "Encountered unexpected state when attempting to extract home-team from JSON! tag_field was expected to be one of {a,b} but instead was '${tag_field}'! Original JSON: ${1}"
    fi
}

#
function extract-home-score-from-row-json {
    echo "${1}" | jq -r '.children[0].children[1].text'
}

function extract-away-score-from-row-json {
    echo "${1}" | jq -r '.children[1].children[1].text'
}

# Extracts only the Away Team Name value from a given schedule row (must be converted to json with pup)
function extract-away-team-from-row-json {
    # If the game is already complete, Home team will be the 2nd child - 1st child if game is upcoming
    # We can check the state for this by looking at the tag of the second child, which will always have a 'tag' field
    # If the tag field is "a", this game is upcoming, if it is "b" it is complete
#    echoerr "DEBUG:away: ${1}"
    local tag_field=$(echo ${1} | jq -r '.children[1].children[1].tag')

    if [[ "${tag_field}" == "a" ]]; then 
        # This game has not occurred yet - Home Team is in the 'text' field of the second element
        echo "${1}" | jq -r '.children[1].children[1].text'
    elif [[ "${tag_field}" == "b" ]]; then 
        # This game is over - Home Team is in the 'text' field of the first element
        echo "${1}" | jq -r '.children[1].children[0].text'
    else 
        echoerr "Encountered unexpected state when attempting to extract home-team from JSON! tag_field was expected to be one of {a,b} but instead was '${tag_field}'! Original JSON: ${1}"
    fi
}

# Extracts the Game Date value from a given schedule row (must be converted to json with pup)
function extract-game-date-from-row-json {
    echo "${1}" | jq -r '.children[2].text'
}

# Extracts the Game Time value from a given schedule row (must be converted to json with pup)
function extract-game-time-from-row-json {
    echo "${1}" | jq -r '.children[3].text'
}

# Extracts only the Game Time value from a given schedule row (must be converted to json with pup)
function extract-game-rink-from-row-json {
    # Text for this row can be either the Rink (if upcoming) or the game result if complete (final,forfeit,overtime)
    # We can key off of the 2nd child, checking which if the gamesheet is full to determine which this is

    local gamesheet_full=$(echo ${1} | jq -r '.children[4].children[1].href' | grep -c 'gamesheet_full.html?gameid=[0-9]*')

    if [[ "${gamesheet_full}" != "1" ]]; then
        echo "${1}" | jq -r '.children[4].children[0].text'
    fi
}

# Adds the given json fieldname and value to the given existing json string, returning the merged string
function add-json-field {
    local existing_json=$1
    local new_field=$2
    local new_value=$3
    echo ${existing_json} | jq --arg "${new_field}" "${new_value}" ". + {${new_field}: \$${new_field}}"
}

# Extracts the gameid from the given row json
function extract-game-id-from-row-json {
    local game_id_href_idx=1
    if [[ "$(is-game-complete "${1}")" == 1 ]]; then
        game_id_href_idx=0
    fi
    # finished game path: '.children[4].children[0].href'
    # future   game path: '.children[4].children[1].href'

    echo "${1}" | jq -r ".children[4].children[${game_id_href_idx}].href" | grep --perl-regexp --only-matching '(?<=.html\?gameid=)[0-9]+'
}

# Extracts all fields from the given row json, generating a standardized JSON object with all fields
function convert-row-json-to-game-json {
    local result_json="{}"

    # Check if root_tag matches expectations - warn if not
    local root_tag=$(echo "${1}" | jq -r '.tag')
    if [[ "${root_tag}" != "tr" ]]; then
        echoerr "WARNING: JSON does not have a root level 'tag' of 'tr'! This is unexpected, and may indicate a problem with the calling implementation"
    fi

    # Check & store if game is complete for use below
    local game_is_complete="$(is-game-complete "${1}")"

    # All Guaranteed Data Together
    local homeTeam="$(extract-home-team-from-row-json "${1}")"
    local awayTeam="$(extract-away-team-from-row-json "${1}")"
    local gameDate="$(extract-game-date-from-row-json "${1}")"
    local gameTime="$(extract-game-time-from-row-json "${1}")"
    local gameId="$(extract-game-id-from-row-json "${1}")"
    result_json=$(echo "${result_json}" | jq ". + {homeTeam: \"${homeTeam}\", awayTeam: \"${awayTeam}\", gameDate: \"${gameDate}\", gameTime: \"${gameTime}\", gameId: ${gameId}}")

    # Conditional Data
    if [[ $game_is_complete == 1 ]]; then
        # Scores
        local homeScore="$(extract-home-score-from-row-json "${1}")"
        local awayScore="$(extract-away-score-from-row-json "${1}")"
        result_json=$(echo "${result_json}" | jq ". + {homeScore: ${homeScore}, awayScore: ${awayScore}}")
    else
        # Rink
        result_json=$(add-json-field "${result_json}" rink "$(extract-game-rink-from-row-json "${1}")")
    fi

    echo "${result_json}" | jq --compact-output
}
### FUNCTIONS

### MAIN ###
### MAIN ###
### MAIN ###

# 1. Request the webpage and filter down to the schedule table
SCHEDULE_TABLE=$(curl --silent --show-error --url "${CLIMAX_SCHEDULE_URL}" | pup 'body div#container div#content div.container div.row.ps_main div.col-md-10 div.row div.col-md-12 div.table-responsive table tbody tr json{}')

# 2. Get number of rows
ROW_COUNT=$(echo "${SCHEDULE_TABLE}" | jq '. | length')

# 3. Set up JSON Schedule
JSON_SCHEDULE='[]'

# 4. Convert each row to JSON
for ((i = 0 ; i < $ROW_COUNT ; i++)); do
    row_json="$(echo "${SCHEDULE_TABLE}" | jq ".[${i}]")"
    game_json="$(convert-row-json-to-game-json "${row_json}")"
    JSON_SCHEDULE="$(echo "${JSON_SCHEDULE}" | jq ". += [${game_json}]")"
done

# Print result
if [[ "${1}" == "compact" || "${1}" == "--compact" || "${1}" == "-c" ]]; then
    JSON_SCHEDULE="$(echo "${JSON_SCHEDULE}" | jq --compact-output)"
fi

echo "$JSON_SCHEDULE"

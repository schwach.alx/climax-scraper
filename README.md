## climax-schedule-scraper
A simple bash program using `curl`, `jq` and `pup` to scrape the PointStreak website for schedule updates. It uses `curl` to get the bare webpage, `pup` to filter down to the HTML schedule table & convert to json, and finally `jq` is used to generate the resulting json string.

### Current Capabilities & Limitations

#### Compact Arg
The script currently accepts only 1 argument (one of [`--compact`, `-c`, `compact`]). If any of these are passed as the 1st argument to the script, the json printed will be minified/compact.

#### Hardcoded `seasonid` & `teamid` Args
Currently, the script has hardcoded the `seasonid` and `teamid` arguments required to get the schedule webpage. Work needs to be done to make it easier for this data to be automatically determined or passed in, as it currently requires manual updates to the script file when a new season starts, or a different team is desired.

### Intentions & Ideas

#### Home Assistant Integration
I intend to work this into my own Home Assistant server with the goal of sending me notifications such as:
- day-of reminders
- pre-game meal reminders
- pre-departure prep alerts
- departure deadline alerts

Could also display games in HASS UIs/Calendars

#### Calendar Integrations
Getting Ice Centre games into calendars has always been a manual undertaking. This could be used to generate ICALs which could then be pushed to a Google Calendar. That calendar could even be shared with teammates.

#### Proof of Concept for Other Scrapers
It would be good to extend this functionality to other websites, such as other rinks. This could be used to notify me every morning of Drop-In or Stick-n-Puck sessions that day, encouraging me to work out and improve my skills. This could eventually manifest as a daily cron/sh job on HASS, which then sends me a report every morning. If extending this platform to multiple other rinks, it would probably be a good idea to switch to some other language such as Go, Rust, Python or Java. Bash is fine, but it's very easy for it to get messed up and lacks type-safety and other features.
